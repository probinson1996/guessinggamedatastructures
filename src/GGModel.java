package src;
public class GGModel 
{
	private int number, guess, high, low, mid;

	
	public GGModel()
	{
		number = 0;
		guess = 0;
		high = 1000;
		mid = 500;
		low = 0;
	}
	public void setNumber(int a)
	{
		number = a;
	}
	public String solveNumber(int a)
	{
		setNumber(a);
		while (low <= high) 
		    {
		        mid = ((high - low) / 2) + low;											
		        if (mid > number) 
		        {    
		        	high = mid - 1;
		        	guess+= 1;
		        	return "The number was less than " + mid;
		        }
		        else
		        {
		        	guess+= 1;
		        }
		        if (mid < number)
		        {
		        	guess+= 1;
		        	low = mid + 1;
		        	return "The number was more than " + mid;
		        }
		        else 
		        {    
		        	guess+= 1;
		        	return "Your number is " + mid + ". The compuer took " + guess + " tries.";
		        }
		    }
		return "The computer failed to find your number";
	}
}