package src;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GGView
{
        private final int FRAME_WIDTH = 400; 
        private final int FRAME_HEIGHT = 250;
        private JFrame mainFrame;
        private JPanel totalGUI, titlePanel, buttonPanel, textPanel, inputPanel;
        private JButton beginButton;
        private JLabel textLabel;
        private JTextField promptInput;
		private JTextArea statusText;
        
        public GGView()
        {
            mainFrame = new JFrame("Guessing Game");
            mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
            mainFrame.setContentPane(createSetUp());
            mainFrame.setVisible(true);
            mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        

        
        public JPanel createSetUp()
        {
        		totalGUI = new JPanel();
        		totalGUI.setLayout(null);
                buttonPanel = new JPanel(new GridLayout());
                inputPanel = new JPanel();
                titlePanel = new JPanel();
                textPanel = new JPanel();
            
 
                titlePanel.setLayout(null);
                titlePanel.setLocation(0, 0);
                titlePanel.setSize(FRAME_WIDTH, 30);
                
                textLabel = new JLabel("INSERT A NUMBER BETWEEN 0 - 1000");
                textLabel.setHorizontalAlignment(0);
                textLabel.setLocation(0, 0);
                textLabel.setSize(FRAME_WIDTH, 30);
                titlePanel.add(textLabel);
             
                promptInput = new JTextField(8);
                inputPanel.add(promptInput);
                inputPanel.setLocation(0, 40);
                inputPanel.setSize(FRAME_WIDTH, 40);
                
                beginButton = new JButton("BEGIN");
                buttonPanel.add(beginButton);
                buttonPanel.setLocation(0, 90);
                buttonPanel.setSize(FRAME_WIDTH, 30);
                
                statusText = new JTextArea("The Computer will begin responding to you here.");
                textPanel.add(statusText);
                textPanel.setLocation(0, 120);
                textPanel.setSize(FRAME_WIDTH, 90);
                
                totalGUI.add(textPanel);
                totalGUI.add(inputPanel);
                totalGUI.add(buttonPanel);
                totalGUI.add(titlePanel);
                totalGUI.setOpaque(true);
                return totalGUI;
        }
        
        
        public void startGame()
        {       
                mainFrame.invalidate();
                buttonPanel.remove(beginButton);
                inputPanel.remove(promptInput);
                totalGUI.add(inputPanel);
                totalGUI.add(buttonPanel);
                mainFrame.validate();
        }
        
   

        
        
        public void addActionListeners(ActionListener listener)
        {       
        	beginButton.setActionCommand("Begin");
            beginButton.addActionListener(listener);
        }
        
        public void setText(String a)
        {
//        	try {
//				Thread.sleep(1 * 1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
        	statusText.setText(a);
        }
        
        
        public JFrame getFrame()
        {
                return mainFrame;
        }
        
        public JPanel getTotalGUI()
        {
                return totalGUI;
        }
        
        public JButton getButton()
        {
                return beginButton;
        }
        public String getNumber()
        {
        	return promptInput.getText();
        }
   
                
}